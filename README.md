# ![Logo](bingster.png) Welcome to Bingster

Bingster is a Python script that takes the current Bing-Wallpaper and makes it your desktop background. It can also display custom text on your wallpaper.

## Installation

Run these commands in a terminal:

```bash
git clone https://codeberg.org/ralfhersel/bingster.git
cd bingster
```

Make these files executable:

> `bingster.py`, `bingster.sh`

Ensure that the path in 'bingster.sh' matches your system.

Ensure that the shebang in 'bingster.py' matches your Python environment.

Check if IMPORT section in 'bingster.py' for additional modules. Should not be necessary because only standard Python modules are required.

## Operation Instructions

Open a terminal and navigate to your bingster directory and run:

```bash
./bingster.sh
```

This will only work if the path in 'bingster.sh' and the shebang in 'bingster.py' is correct.

You should see the latest Bing-Wallpaper as background image on your desktop.

If it does not work, check the output of 'bingster.py' in your terminal and make the required corrections.

Instead of running the shell script, you can also run 'bingster.py' directly with:

```bash
python3 bingster.py
or
python bingster.py
```

That depends if 'python3' or 'python' is used in your distribution to start Python3.

If everything works well, you can include 'bingster.sh' in your Autostart directory or settings to enable an updated wallpaper whenever you boot your system.

## Configuration

Bingster has no separate configuration file. Instead, all configurations can be done in the **CONSTANTS** section of the 'bingster.py' file. All settings should be self-explanatory; read the comments on the right side of each line. These are the most important settings:

- NAME_ON_IMAGE = [True/False]
  Set it to **True** if you want to display custom text on the wallpaper.

- GNOME_SHELL = [True/False]
  Set it to **True** if you are using the GNOME-Desktop-Environment. In this case Bingster will use an API-method to set the wallpaper. If set to **False**, Bingster will only download the wallpaper, but not enable it as a background wallpaper.

- FONT_SIZE = [integer]
  This is the pixel size of the text displayed on the wallpaper, if NAME_ON_IMAGE = True.

- TEXT_POSITION = [NorthWest, North, NorthEast, West, Center, East, SouthWest, South, SouthEast]
  This setting determines, at which position your custom text is displayed on the screen, if NAME_ON_IMAGE = True.

- TEXT_COLOR = [imagemagick colors]
  The color of your text on the wallpaper, if NAME_ON_IMAGE = True.

## Text on Wallpaper

If you set **NAME_ON_IMAGE = True** you can display custom texts on top of the Bing-wallpaper. Here are some use case for this option:

- Learn vocabulary

- Words of motivation

- Definitions of complicated words

This option requires a text file **words.txt** in your Bingster-directory. Just fill it up with the words, that you want to display on the wallpager. Whenever Bingster is started, it takes a random line of your words file to display it.

## Release Notes

###### Version 0.23, 2023.03.25

* Removed Bitcoin price display
* Added custom word display on the wallpaper
* Removed a lot of old, unnecessary code

###### Version 0.22, 2022.05.18

* Gnome 42 dark theme supported when setting wallpaper

###### Version 0.21, 2019.11.17

* Latest version. Older release notes are not available.

###### Older Versions

* not documented

## License

Author: Ralf Hersel

License: GPL3

Repository: https://codeberg.org/ralfhersel/bingster.git

## Contact

https://matrix.to/#/@ralfhersel:fsfe.org
