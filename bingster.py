#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
Name:           bingster.py
Description:    Show the bing background image as wallpager with text on it
Author:         Ralf Hersel - ralf.hersel@gmx.net
License:        GPL3
Date:           2023-03-25
Version:        0.23
"""

# === IMPORT ===================================================================

import urllib.request                                                           # for web page fetching (maybe obsolete because of API
import subprocess                                                               # for ImageMagick (text on wallpaper)
import os
import json                                                                     # for bing API
from gi.repository import Gio                                                   # for Wallpaper setting
import random                                                                   # for shuffeling the words list

# === CONSTANTS ================================================================

IMG_SOURCE = "http://www.bing.com"                                              # Webpage where the image is taken from
IMG_FILENAME = "bingster.jpg"                                                   # Filename of the image file
FETCH_METHOD = "api"                                                            # api or web
IMAGE_API = "https://www.bing.com/HPImageArchive.aspx?format=js&idx=0&n=1&mkt=en-US"
NAME_ON_IMAGE = True                                                            # Show name as text in image
GNOME_SHELL = True                                                              # Activates wallpaper via Gsettings, if False, image file only
IMAGE_PATH = os.path.expanduser('~') + '/Bilder/'                               # Image path for Gnome-Shell '/home/user/Bilder/'
FONT_SIZE = 50                                                                  # Font size of in-image text
TEXT_POSITION = 'South'                                                         # NorthWest, North, NorthEast, West, Center, East, SouthWest, South, SouthEast
TEXT_COLOR = 'white'                                                            # Color of on-image text: white, yellow, ...

# === FEATURES =================================================================

def get_image_url(page):                                                        # get url of the bing background image
    img_search = '<link id="bgLink" rel="preload" href="'                       # search pattern in Bing page to find the background image
    offset = len(img_search)                                                    # length of start offset
    start = page.find(img_search, 0) + offset                                   # get start position of image url
    end = page.find("jpg", start)                                               # get end position of image url
    image_name = page[start:end+3]                                              # get image name
    image_name = image_name.replace('\\', '')                                   # clean string
    return image_name

def get_image_api():                                                            # get image from Bing API
    data = urllib.request.urlopen(IMAGE_API).read()                             # retrieve Image API
    data = data.decode("utf-8")
    j = json.loads(data)                                                        # convert json to dict
    image_filename = j['images'][0]['urlbase']
    image_title = j['images'][0]['title']
    image_filename = image_filename + '_1920x1080.jpg'
    return image_filename, image_title

def activate_wallpaper(schema, image_path):                                     # activate wallpaper in Gnome Shell
    gsettings = Gio.Settings.new(schema)
    gsettings.set_string('picture-uri', image_path)
    if schema == 'org.gnome.desktop.background':
        gsettings.set_string('picture-uri-dark', image_path)
    gsettings.set_string('picture-options', 'zoom')
    Gio.Settings.sync()
    gsettings.apply()

def get_real_name(image_name):                                                  # extract real image name from image link
    start = image_name.find('.', 0)
    end = image_name.find('_', start)
    real_name = image_name[start+1:end]
    count = 0
    spaced_real_name = ''
    for character in real_name:                                                 # loop the name
        count += 1
        if count > 1 and character.isupper():                                   # skip first char and get uppercase letters
            spaced_real_name += ' ' + character                                 # add a space before uppercase letters
        elif character.isnumeric():                                             # skip numbers
            pass                                                                # do nothing
        else:
            spaced_real_name += character                                       # just add the first or lowercase letter
    return spaced_real_name

def add_text_to_image(the_text, pointsize, position, color):                    # draw text on grey background frame
    image_path = IMAGE_PATH + IMG_FILENAME
    action = []
    action.append('convert')
    action.append(image_path)
    action.append('-gravity')
    action.append(position)
    action.append('-pointsize')
    action.append(str(pointsize))    
    action.append('-stroke')
    action.append('#000C')
    action.append('-strokewidth')
    action.append('2')
    action.append('-annotate')
    action.append('0')
    action.append(the_text)
    action.append('-stroke')
    action.append('none')
    action.append('-fill')
    action.append(color)
    action.append('-annotate')
    action.append('0')
    action.append(the_text)
    action.append(image_path)
    process = subprocess.Popen(action)                                          # execute the imagemagick command
    process.wait()                                                              # wait for subprocess to finish

def save_image_name(image_name, real_name):                                     # save image path and name to file
    f = open('name.txt', 'w')                                                   # open file for image name
    f.write(image_name + '\n')                                                  # write name of image to text file
    f.write(real_name + '\n')                                                   # write real of image to text file
    f.close()                                                                   # close file
    print('Real name :', real_name)                                             # print uncluttered image name
    
def get_words():                                                                # read words.txt and return a list
    words = []
    with open('words.txt') as f:
        for line in f:
            word = line.strip()                                                 # remove trailing line break
            words.append(word)
    random.shuffle(words)                                                       # shuffle all entries in list
    return words

# === MAIN =====================================================================

def main(args):
    if FETCH_METHOD == 'api':                                                   # FETCH_METHOD == 'api'
        image_name, real_name = get_image_api()                                 # read api
    elif FETCH_METHOD == 'web':                                                 # FETCH_METHOD == 'web'
        page = urllib.request.urlopen(IMG_SOURCE).read()                        # read web page
        page = str(page)                                                        # convert to Unicode-String
        image_name = get_image_url(page)                                        # get url of the bing background image
        real_name = get_real_name(image_name)                                   # try to remove clutter from image name
    else:
        print('Error: no fetch method given')                                   # probably wrong setting of constant FETCH_METHOD
        exit(1)                                                                 # stop execution with error
    
    save_image_name(image_name, real_name)                                      # save image path and name to file
    
    url = IMG_SOURCE + image_name                                               # get image url
    image_path = IMAGE_PATH + IMG_FILENAME                                      # store wallpaper in Gnome's image folder
    urllib.request.urlretrieve(url, image_path)                                 # save image file
    
    if NAME_ON_IMAGE:                                                           # this has first priority (bitcoin will be suppressed
        words = get_words()                                                     # load words in a list
        add_text_to_image(words[0], FONT_SIZE, TEXT_POSITION, TEXT_COLOR)       # add image name as text to image
    
    if GNOME_SHELL:                                                             # activate wallpaper via Gsettings
        activate_wallpaper('org.gnome.desktop.background', image_path)
        activate_wallpaper('org.gnome.desktop.screensaver', image_path)
    
    return 0

if __name__ == '__main__':
    import sys
    sys.exit(main(sys.argv))    
